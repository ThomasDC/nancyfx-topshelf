﻿using System.IO;
using Nancy;
using Nancy.ErrorHandling;

namespace Web
{
    public class PageNotFoundHandler : IStatusCodeHandler
    {
        private readonly IRootPathProvider _rootPathProvider;

        public PageNotFoundHandler(IRootPathProvider rootPathProvider)
        {
            _rootPathProvider = rootPathProvider;
        }

        public bool HandlesStatusCode(HttpStatusCode statusCode, NancyContext context)
        {
            return statusCode == HttpStatusCode.NotFound;
        }

        public void Handle(HttpStatusCode statusCode, NancyContext context)
        {
            context.Response.Contents = stream =>
            {
                var filename = Path.Combine(_rootPathProvider.GetRootPath(), "Views/PageNotFound.html");
                using (var file = File.OpenRead(filename))
                {
                    file.CopyTo(stream);
                }
            };
        }
    }
}
