﻿using System;
using Common;
using Common.Logging;
using Nancy.Hosting.Self;

namespace Web
{
    public class WebService : IHostable
    {
        private readonly NancyHost _nancyHost;

        public WebService()
        {
            var uri = new Uri("http://localhost:3579");
            _nancyHost = new NancyHost(uri);
        }

        public void Start()
        {
            Logger.Current.Info("Starting Nancy WebService");
            _nancyHost.Start();
        }

        public void Stop()
        {
            Logger.Current.Info("Stopping Nancy WebService");
            _nancyHost.Stop();
        }
    }
}