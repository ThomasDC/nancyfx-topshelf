﻿using System;
using Common;
using Common.Logging;
using Nancy;

namespace Web
{
    public class SampleModule : NancyModule
    {
        private readonly IDoSomething _doSomething;
        private readonly IDoAnotherThing _doAnotherThing;
        private readonly IBackendService _backendService;

        public SampleModule(IDoSomething doSomething, IDoAnotherThing doAnotherThing, IBackendService backendService)
        {
            _doSomething = doSomething;
            _doAnotherThing = doAnotherThing;
            _backendService = backendService;

            Get["/"] = _ => "Hello World! " + _doSomething.DoIt(42);
            Get["/view/{name}"] = _ => View["main.html", _.name];
            Get["/count"] = _ =>
            {
                Logger.Current.Info("/count was called");
                return string.Format("Time has elapsed {0} times", _backendService.FetchCount());
            };
            Get["/greet/{name}"] = x =>
            {
                return string.Concat("Hello ", x.name, ": " + _doAnotherThing.DoIt("123"), ": " + _doAnotherThing.DoIt("123"));
            };
            Get["/v1/feeds"] = parameters =>
            {
                var feeds = new[] { "foo", "bar" };
                return Response.AsJson(feeds);
            };
        }
    }
}
