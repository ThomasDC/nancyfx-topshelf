﻿using System;
using System.Timers;
using Common;
using Common.Logging;

namespace Service
{
    public class BackendService : IHostable, IBackendService
    {
        private readonly Timer _timer;
        private int _count;

        public BackendService()
        {
            _timer = new Timer(1000) {AutoReset = true};
            _timer.Elapsed += (sender, eventArgs) =>
            {
                Logger.Current.InfoFormat("It is {0} and all is well", DateTime.Now);
                _count++;
            };
        }

        public void Start()
        {
            Logger.Current.Info("Starting BackendService");
            _timer.Start();
        }

        public void Stop()
        {
            Logger.Current.Info("Stopping BackendService");
            _timer.Stop();
        }

        public int FetchCount()
        {
            return _count;
        }
    }
}