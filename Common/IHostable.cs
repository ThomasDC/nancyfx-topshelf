﻿namespace Common
{
    public interface IHostable
    {
        void Start();
        void Stop();
    }
}