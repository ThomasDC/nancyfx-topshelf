﻿namespace Common
{
    public interface IDoSomething
    {
        string DoIt(int input);
    }
}