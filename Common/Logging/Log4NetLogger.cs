﻿using System;
using log4net.Config;

namespace Common.Logging
{
    public class Log4NetLogger : Logger
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger("root");

        static Log4NetLogger()
        {
            XmlConfigurator.Configure();
        }

        public override void Debug(object message)
        {
            Log.Debug(message);
        }

        public override void Debug(object message, Exception exception)
        {
            Log.Debug(message, exception);
        }

        public override void DebugFormat(string format, params object[] args)
        {
            Log.DebugFormat(format, args);
        }

        public override void Info(object message)
        {
            Log.Info(message);
        }

        public override void Info(object message, Exception exception)
        {
            Log.Info(message, exception);
        }

        public override void InfoFormat(string format, params object[] args)
        {
            Log.InfoFormat(format, args);
        }

        public override void Warn(object message)
        {
            Log.Warn(message);
        }

        public override void Warn(object message, Exception exception)
        {
            Log.Warn(message, exception);
        }

        public override void WarnFormat(string format, params object[] args)
        {
            Log.WarnFormat(format, args);
        }

        public override void Error(object message)
        {
            Log.Error(message);
        }

        public override void Error(object message, Exception exception)
        {
            Log.Error(message, exception);
        }

        public override void ErrorFormat(Exception exception, string format, params object[] args)
        {
            Log.Error(string.Format(format, args), exception);
        }

        public override void ErrorFormat(string format, params object[] args)
        {
            Log.ErrorFormat(format, args);
        }

        public override void Fatal(object message)
        {
            Log.Fatal(message);
        }

        public override void Fatal(object message, Exception exception)
        {
            Log.Fatal(message, exception);
        }

        public override void FatalFormat(string format, params object[] args)
        {
            Log.FatalFormat(format, args);
        }
    }
}