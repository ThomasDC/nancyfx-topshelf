﻿using System;

namespace Common.Logging
{
    public class NullLogger : Logger
    {
        public override void Debug(object message)
        {
        }

        public override void Debug(object message, Exception exception)
        {
        }

        public override void DebugFormat(string format, params object[] args)
        {
        }

        public override void Info(object message)
        {
        }

        public override void Info(object message, Exception exception)
        {
        }

        public override void InfoFormat(string format, params object[] args)
        {
        }

        public override void Warn(object message)
        {
        }

        public override void Warn(object message, Exception exception)
        {
        }

        public override void WarnFormat(string format, params object[] args)
        {
        }

        public override void Error(object message)
        {
        }

        public override void Error(object message, Exception exception)
        {
        }

        public override void ErrorFormat(string format, params object[] args)
        {
        }

        public override void ErrorFormat(Exception exception, string format, params object[] args)
        {
        }

        public override void Fatal(object message)
        {
        }

        public override void Fatal(object message, Exception exception)
        {
        }

        public override void FatalFormat(string format, params object[] args)
        {
        }
    }
}