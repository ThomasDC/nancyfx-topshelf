﻿using System;

namespace Common.Logging
{
    public abstract class Logger
    {
        private static Logger _current;

        static Logger()
        {
            _current = new NullLogger();
        }

        public static Logger Current
        {
            get { return _current; }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                _current = value;
            }
        }

        public static void ResetToDefault()
        {
            _current = new NullLogger();
        }

        public abstract void Debug(object message);
        public abstract void Debug(object message, Exception exception);
        public abstract void DebugFormat(string format, params object[] args);
        public abstract void Info(object message);
        public abstract void Info(object message, Exception exception);
        public abstract void InfoFormat(string format, params object[] args);
        public abstract void Warn(object message);
        public abstract void Warn(object message, Exception exception);
        public abstract void WarnFormat(string format, params object[] args);
        public abstract void Error(object message);
        public abstract void Error(object message, Exception exception);
        public abstract void ErrorFormat(string format, params object[] args);
        public abstract void ErrorFormat(Exception exception, string format, params object[] args);
        public abstract void Fatal(object message);
        public abstract void Fatal(object message, Exception exception);
        public abstract void FatalFormat(string format, params object[] args);
    }
}