﻿namespace Common
{
    public interface IDoAnotherThing
    {
        int DoIt(string input);
    }
}