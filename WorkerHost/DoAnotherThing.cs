﻿using Common;
using Common.Logging;

namespace Host
{
    public class DoAnotherThing : IDoAnotherThing
    {
        private int _count = 0;

        public int DoIt(string input)
        {
            Logger.Current.Info("DoAnotherThing did it!");
            return _count++;
        }
    }
}