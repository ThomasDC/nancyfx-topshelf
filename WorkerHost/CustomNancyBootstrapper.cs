﻿using Castle.Windsor;
using Nancy.Bootstrappers.Windsor;
using Nancy.Conventions;

namespace Host
{
    public class CustomNancyBootstrapper : WindsorNancyBootstrapper
    {
        protected override IWindsorContainer GetApplicationContainer()
        {
            // Tells Nancy to use our container instead of the custom built-in TinyIOC container
            return Program.Container;
        }
        protected override void ConfigureConventions(NancyConventions conventions)
        {
            base.ConfigureConventions(conventions);

            conventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory("assets", @"Scripts"));
        }
    }
}