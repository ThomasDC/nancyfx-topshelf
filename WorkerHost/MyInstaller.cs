﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Common;
using Nancy.Bootstrappers.Windsor;
using Service;
using Web;

namespace Host
{
    public class MyInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<HostRoot>().LifestyleSingleton(),
                Component.For<IHostable, IBackendService>().ImplementedBy<BackendService>().LifestyleSingleton(),
                Component.For<IHostable>().ImplementedBy<WebService>().LifestyleSingleton(),
                Component.For<IDoSomething>().ImplementedBy<DoSomething>().LifestyleSingleton(),
                Component.For<IDoAnotherThing>().ImplementedBy<DoAnotherThing>().LifestyleScoped<NancyPerWebRequestScopeAccessor>());
        }
    }
}