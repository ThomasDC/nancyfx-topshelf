﻿using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using Castle.Windsor;
using Castle.Windsor.Installer;
using Common.Logging;
using Topshelf;

namespace Host
{
    class Program
    {
        public static WindsorContainer Container;

        static void Main()
        {
            Logger.Current = new Log4NetLogger();

            Container = new WindsorContainer();
            Container.Kernel.Resolver.AddSubResolver(new CollectionResolver(Container.Kernel));
            Container.Install(FromAssembly.This());

            var host = HostFactory.New(x =>
            {
                x.UseLog4Net();
                x.Service<HostRoot>(s =>
                {
                    s.ConstructUsing(_ => Container.Resolve<HostRoot>());
                    s.WhenStarted(_ => _.Start());
                    s.WhenStopped(_ =>
                    {
                        _.Stop();
                        Container.Release(_);
                        Container.Dispose();
                    });
                });
                x.StartAutomatically();
                x.SetServiceName("alternative-dotnet");
                x.RunAsLocalSystem();
            });
            
            host.Run();
        }
    }
}
