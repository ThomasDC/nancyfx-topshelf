﻿using System.Collections.Generic;
using Common;
using Common.Logging;

namespace Host
{
    public class HostRoot
    {
        private readonly IList<IHostable> _hostables;

        public HostRoot(IList<IHostable> hostables)
        {
            _hostables = hostables;
        }

        public void Start()
        {
            Logger.Current.Info("Starting HostRoot");
            foreach (var hostable in _hostables)
            {
                hostable.Start();
            }
        }

        public void Stop()
        {
            Logger.Current.Info("Stopping HostRoot");
            foreach (var hostable in _hostables)
            {
                hostable.Stop();
            }
        }
    }
}