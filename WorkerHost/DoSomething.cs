﻿using Common;
using Common.Logging;

namespace Host
{
    public class DoSomething : IDoSomething
    {
        private int _count;

        public DoSomething()
        {
            Logger.Current.Debug("DoSometing was created!");
            _count = 0;
        }

        public string DoIt(int input)
        {
            Logger.Current.Warn("DoSometing was called!");
            return string.Format("Done: {0}! ({1}nth time)", input, _count++);
        }
    }
}